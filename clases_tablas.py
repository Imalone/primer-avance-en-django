from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator

User = get_user_model

class Usuario(models.Model):
	idUsuario = models.IntegerField(unique=True)
	claseUsuario = models.CharField('Asministrativo_Docente_Obrero', max_length=45)
	nombre = models.CharField('Nombre de usuario', max_length=45)
	clave = models.CharField('Clave de usuario', max_length=45)
	estado = models.CharField('Activo_Inavtivo_Retirado', max_length=45)
	date = models.DateField('Fecha de registro')

class Trabajador(models.Model):
	idTrabajador = models.IntegerField(unique=True)
	nombre = models.CharField('Nombre del trabajador', max_length=45)
	apellido = models.CharField('Apellido del trabajador', max_length=45)
	fechaNac = models.DateField('Fecha de nacimiento')
	genero = models.CharField('Femenino o mascilino', max_length=45)
	nacionalidad = models.CharField('Venezolano o Extranjero', max_length=45)
	estadoCivil = models.CharField('Soltero_Casado_Viudo_Concubino', max_length=45)
	date = models.DateField('Fecha de registro')

class Personal(models.Model):
	idPersonal = models.IntegerField(unique=True)
	afiliado = models.CharField('Asministrativo_Docente_Obrero', max_length=45)
	noAfiliado = models.CharField('Asministrativo_Docente_Obrero', max_length=45)
	cargo = models.CharField('A que se dedica', max_length=45)
	descripcion = models.CharField('Descripcion del cargo', max_length=45)
	date = models.DateField('Fecha de registro')

class DireccTel(models.Model):
	idDirectel = models.IntegerField(unique=True)
	edoMunParr = models.CharField('Estado_Municipio_Parroquia', max_length=45)
	lugar = models.CharField('Sector o barrio', max_length=45)
	vivienda = models.CharField('Casa o edificio', max_length=45)
	telefono = models.IntegerField('Numero telefonico')
	email = models.CharField('Direccion de email', max_length=45)
	redesS = models.CharField('Redes sociales', max_length=45)
	date = models.DateField('Fecha de registro')

class Sede(models.Model):
	idSede = models.IntegerField(unique=True)
	sede = models.CharField('Central_Enfermeria_Preescolar_StaMaria', max_length=45)
	departamento = models.CharField('Departamento al que pertenece', max_length=45)
	edificio = models.CharField('Administrativo_Academico', max_length=45)
	piso = models.IntegerField('Numero de piso')
	date = models.DateField('Fecha de registro')

class Uniforme(models.Model):
	idUniforme = models.IntegerField(unique=True)
	camisa = models.CharField(max_length=45)
	pantalon = models.CharField(max_length=45)
	calzado = models.CharField(max_length=45)
	chaqueta = models.CharField(max_length=45)
	date = models.DateField('Fecha de registro')

class NiveldeInstruccion(models.Model):
	idNivInst = models.IntegerField(unique=True)
	Estudios = models.CharField('Primaria_Secundaria_Bachillerato_Universidad_Posgrado', max_length=45)
	cursos = models.CharField('Cursos realizados', max_length=45)
	date = models.DateField('Fecha de registro')

class TranspoteVivienda(models.Model):
	idTransViv = models.IntegerField(unique=True)
	mediosTransporte = models.CharField('Publico_Privado_Metro_Propio', max_length=45)
	vivienda = models.CharField('Propia_Alquilada_Hacinada', max_length=45)
	tipo = models.CharField('Casa_Apartamento_Rancho', max_length=45)
	techo = models.CharField('Zinc_Platabanba_Teja_Acerolit', max_length=45)
	paredes = models.CharField('Cemento_Bloque_Zinc_Adobe', max_length=45)
	piso = models.CharField('Cemento_Ceramica_Granito_Tierra', max_length=45)
	aguasServidas = ('Diga como se deshace de las aguas servidas')
	aguasBlancas = ('Tuberias_Cisterna_Pozo')
	date = models.DateField('Fecha de registro')

class EquiposMedicos(models.Model):
	idEquipMed = models.IntegerField(unique=True)
	nombre = models.CharField('Nombre del o los equipos', max_length=45)
	tipo = models.CharField('Indique el tipo de equipo', max_length=45)
	date = models.DateField('Fecha de registro')

class CargaFamiliar(models.Model):
	idCargaFam = models.IntegerField(unique=True)
	nombre = models.CharField('Publico_Privado_Metro_Propio', max_length=45)
	apellido = models.CharField('Propia_Alquilada_Hacinada', max_length=45)
	cedula = models.CharField('Casa_Apartamento_Rancho', max_length=45)
	fechaNac = models.CharField(max_length=45)
	paretesco = models.CharField('Madre_Padre_Tio_Abuelo_Sobrino', max_length=45)
	educacion = models.CharField('Redes sociales', max_length=45)
	date = models.DateField('Fecha de registro')

class Actividad(models.Model):
	idActividad = models.IntegerField(unique=True)
	cultural = models.CharField('Publico_Privado_Metro_Propio', max_length=45)
	deportiva = models.CharField('Propia_Alquilada_Hacinada', max_length=45)
	date = models.DateField('Fecha de registro')

class Medicamentos(models.Model):
	idMedicamentos = models.IntegerField(unique=True)
	nombre = models.CharField('Publico_Privado_Metro_Propio', max_length=45)
	accionTerapeutica= models.CharField('Casa_Apartamento_Rancho', max_length=45)
	presentacion = models.CharField('Madre_Padre_Tio_Abuelo_Sobrino', max_length=45)
	date = models.DateField('Fecha de registro')

class CargaFamMedicamentos(models.Model):
	idCargaFamMed = models.IntegerField(unique=True)
	nombre = models.CharField('Publico_Privado_Metro_Propio', max_length=45)
	date = models.DateField('Fecha de registro')

class TrabajadorCargaFam(models.Model):
	idTrabajadorCargaFam = models.IntegerField(unique=True)
	nombre = models.CharField('Publico_Privado_Metro_Propio', max_length=45)
	date = models.DateField('Fecha de registro')

class CargaFamEquiposMed(models.Model):
	idCargaFamEqMed = models.IntegerField(unique=True)
	nombre = models.CharField('Nombre o nombres', max_length=45)
	date = models.DateField('Fecha de registro')

class Organizacion(models.Model):
	idOrganizacion = models.IntegerField(unique=True)
	nombre = models.CharField('Nombre o nombres', max_length=45)
	rif = models.CharField('Rif de la organizacion', max_length=45)
	ubicacion = models.CharField('Ubicacion de la organizacion', max_length=45)
	date = models.DateField('Fecha de registro')
	
